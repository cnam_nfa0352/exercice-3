package fr.cnam.foad.nfa035.fileutils.streaming.serializer;

import com.github.dockerjava.zerodep.shaded.org.apache.commons.codec.binary.Base64InputStream;
import fr.cnam.foad.nfa035.fileutils.streaming.media.AbstractImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    private final OutputStream sourceOutputStream;

    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    @Override
    public InputStream getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }


}
