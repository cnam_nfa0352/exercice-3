package fr.cnam.foad.nfa035.fileutils.simpleaccess.test;

import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

public class SimpleAccessTest {

    private static final Logger LOGGER = LogManager.getLogger(SimpleAccessTest.class);

    /**
     * Méthode utile pour afficher une image sérialisée
     *
     * @param str
     * @param chars
     * @return
     */
    private static String splitDisplay(String str, int chars) {
        StringBuffer strBuf = new StringBuffer();
        int i = 0;
        strBuf.append("================== Affichage de l'image encodée en Base64 ==================\n");
        while (i + chars < str.length()) {
            strBuf.append(str, i, i += chars);
            strBuf.append("\n");
        }
        strBuf.append(str.substring(i));
        strBuf.append("\n================================== FIN =====================================\n");

        return strBuf.toString();
    }

    @Test
    public void test() {

        try {
            File image = new File("petite_image.png");
            ImageSerializer serializer = new ImageSerializerBase64Impl();

            // Sérialization
            String encodedImage = (String) serializer.serialize(image);
            LOGGER.info(splitDisplay(encodedImage, 76));

            // Désérialisation
            byte[] deserializedImage = (byte[]) serializer.deserialize(encodedImage);

            // Vérifications
            //  1/ Automatique
            assert (Arrays.equals(deserializedImage, Files.readAllBytes(image.toPath())));
            LOGGER.info("Cette sérialisation est bien réversible :)");
            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            new FileOutputStream(extractedImage).write(deserializedImage);
            LOGGER.info("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
